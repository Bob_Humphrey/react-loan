import React, { Component } from "react";
import TextBoxWidget from "./components/TextBoxWidget";
import SelectWidget from "./components/SelectWidget";
import "./css/tailwind.css";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loanAmount: 0,
      loanAmountDisplay: "",
      loanAmountError: false,
      loanRate: 3.0,
      loanYears: 30,
      statusMessage: "Enter the loan amount"
    };
  }

  handleChange = changeEvent => {
    switch (changeEvent.target.name) {
      case "loanAmount":
        let modLoanAmount = changeEvent.target.value.replace(/,/g, "");
        let statusMessage, newLoanAmountDisplay;
        if (changeEvent.target.value.length > 11) {
          statusMessage = "The loan amount must be 11 characters or less";
          newLoanAmountDisplay = this.state.loanAmountDisplay;
          modLoanAmount = this.state.loanAmount;
        } else if (modLoanAmount < 1000) {
          statusMessage = "The loan amount must be at least 1000";
          newLoanAmountDisplay = changeEvent.target.value;
        } else {
          statusMessage = "No message";
          newLoanAmountDisplay = changeEvent.target.value;
        }
        this.setState({
          loanAmountDisplay: newLoanAmountDisplay,
          statusMessage: statusMessage
        });
        this.setState(
          {
            loanAmount: modLoanAmount
          },
          () => {
            this.validateField("loanAmount", modLoanAmount);
          }
        );
        break;
      case "loanRate":
        this.setState({ loanRate: changeEvent.target.value });
        break;
      case "loanYears":
        this.setState({ loanYears: changeEvent.target.value });
        break;
      default:
        break;
    }
  };

  validateField(fieldName, value) {
    let statusMessage = this.state.statusMessage;
    let loanAmountError = this.state.loanAmountError;

    switch (fieldName) {
      case "loanAmount":
        loanAmountError = !value.match("^[0-9]*$");
        statusMessage = loanAmountError ? "Numbers only please" : statusMessage;
        break;
      default:
        break;
    }

    this.setState({
      statusMessage: statusMessage,
      loanAmountError: loanAmountError
    });
  }

  render() {
    let monthlyPayment,
      totalPayment,
      displayMonthlyPayment,
      displayTotalPayment;
    const p = this.state.loanAmount;
    const r = (this.state.loanRate * 0.01) / 12;
    const n = this.state.loanYears * 12;
    if (p < 1000) {
      displayMonthlyPayment = "";
      displayTotalPayment = "";
    } else {
      monthlyPayment = p * ((r * (1 + r) ** n) / ((1 + r) ** n - 1));
      totalPayment = monthlyPayment * n;
      displayMonthlyPayment = monthlyPayment.toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });
      displayTotalPayment = totalPayment.toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });
    }
    if (isNaN(monthlyPayment)) {
      displayMonthlyPayment = "";
    }
    if (isNaN(totalPayment)) {
      displayTotalPayment = "";
    }

    let statusColor = "text-black";
    let loanAmountBgColor = "bg-grey-light";
    let loanAmountLabelBgColor = "bg-grey";
    if (this.state.statusMessage === "No message") {
      statusColor = "text-white";
    }
    if (this.state.loanAmountError) {
      statusColor = "text-red";
      loanAmountBgColor = "bg-red-lighter";
      loanAmountLabelBgColor = "bg-red-lighter";
    }

    return (
      <div className="bg-white">
        <header className="flex justify-center mb-4 py-12 lg:py-8 bg-grey-lighter">
          <h1 className="font-normal text-orange-dark text-2xl lg:text-3xl m-0 p-0">
            Loan Payment Calculator
          </h1>
        </header>

        <div className="flex justify-center w-full mt-4 lg:mt-16 text-xl">
          <div
            className={
              "w-5/6 px-4 py-1 flex justify-center items-center " + statusColor
            }
          >
            {this.state.statusMessage}
          </div>
        </div>

        <div className="flex justify-center w-full mt-4 text-xl lg:text-3xl">
          <div className="flex w-5/6 justify-center">
            <div className="w-full lg:w-3/4 justify-center">
              <div className="lg:flex justify-center w-full">
                <div
                  className={
                    "w-full lg:w-1/2 h-16 px-4 flex justify-center items-center lg:border-r-8 border-white " +
                    loanAmountLabelBgColor
                  }
                >
                  Loan Amount
                </div>
                <TextBoxWidget
                  name="loanAmount"
                  loanAmount={this.state.loanAmountDisplay}
                  onChange={this.handleChange}
                  bgColor={loanAmountBgColor}
                />
              </div>

              <div className="lg:flex justify-center w-full mt-2">
                <div className="w-full lg:w-1/2 h-16 px-4 bg-grey flex justify-center items-center lg:border-r-8 border-white">
                  Interest Rate
                </div>
                <SelectWidget
                  name="loanRate"
                  options={[
                    "3.00",
                    "3.25",
                    "3.50",
                    "3.75",
                    "4.00",
                    "4.25",
                    "4.50",
                    "4.75",
                    "5.00",
                    "5.25",
                    "5.50",
                    "5.75",
                    "6.00",
                    "6.25",
                    "6.50",
                    "6.75"
                  ]}
                  value={this.state.loanRate}
                  onChange={this.handleChange}
                />
              </div>

              <div className="lg:flex justify-center w-full mt-2">
                <div className="w-full lg:w-1/2 h-16 px-4 bg-grey flex justify-center items-center lg:border-r-8 border-white">
                  Loan Years
                </div>
                <SelectWidget
                  name="loanYears"
                  options={[
                    30,
                    29,
                    28,
                    27,
                    26,
                    25,
                    24,
                    23,
                    22,
                    21,
                    20,
                    19,
                    18,
                    17,
                    16,
                    15,
                    14,
                    13,
                    12,
                    11,
                    10,
                    9,
                    8,
                    7,
                    6,
                    5,
                    4,
                    3,
                    2,
                    1
                  ]}
                  value={this.state.loanYears}
                  onChange={this.handleChange}
                />
              </div>

              <div className="lg:flex justify-center w-full mt-2">
                <div className="w-full lg:w-1/2 h-16 px-4 bg-grey flex justify-center items-center lg:border-r-8 border-white">
                  Monthly Payment
                </div>
                <div className="w-full lg:w-1/2 h-16 bg-grey-light flex justify-center items-center">
                  {displayMonthlyPayment}
                </div>
              </div>

              <div className="lg:flex justify-center w-full mt-2 mb-16">
                <div className="w-full lg:w-1/2 h-16 px-4 bg-grey flex justify-center items-center lg:border-r-8 border-white">
                  Total Amount Repaid
                </div>
                <div className="w-full lg:w-1/2 h-16 bg-grey-light flex justify-center items-center ">
                  {displayTotalPayment}
                </div>
              </div>
            </div>
          </div>
        </div>

        <footer className="flex justify-center w-full py-10">
          <a href="https://bob-humphrey.com">
            <img
              className="w-16 border-2 border-white"
              src={require("./images/bh-logo-grey.gif")}
              alt="Bob Humphrey website "
            />
          </a>
        </footer>
      </div>
    );
  }
}

export default App;
