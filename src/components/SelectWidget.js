import React, { Component } from 'react';

class SelectWidget extends Component {

  render() {
    const options = this.props.options
    return (
      <select
        className="w-full lg:w-1/2 h-16 bg-grey-light text-center"
        name={this.props.name}
        value={this.props.value}
        onChange={this.props.onChange}>
        {
          options.map(function(option, i) {
            return <option key={i}
              value={option}>{option}</option>
          })
        }
      </select>
    );
  }
}

export default SelectWidget;
