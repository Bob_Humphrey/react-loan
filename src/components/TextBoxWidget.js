import React, { Component } from 'react';

class TextBoxWidget extends Component {

  render() {
    return (
      <input
        className={"w-full lg:w-1/2 h-16 text-center " + this.props.bgColor}
        type="text"
        name={this.props.name}
        value={this.props.loanAmount}
        onChange={this.props.onChange}
      />
    );
  }
}

export default TextBoxWidget;
