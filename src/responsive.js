// BEFORE

<div className="flex justify-center w-full mt-4 text-3xl">
  <div className="w-3/4 flex">
    <div className={"w-1/2 h-16 px-4 flex justify-center items-center border-r-8 border-white " + loanAmountLabelBgColor}>
      Loan Amount
    </div>
    <TextBoxWidget name="loanAmount" loanAmount={this.state.loanAmountDisplay} onChange={this.handleChange} bgColor={loanAmountBgColor}/>
  </div>
</div>

// BEFORE TEXT BOX

class TextBoxWidget extends Component {

  render() {
    return (
      <input
        className={"w-1/2 h-16 text-center " + this.props.bgColor}
        type="text"
        name={this.props.name}
        value={this.props.loanAmount}
        onChange={this.props.onChange}
      />
    );
  }
}

// AFTER

<div className="flex justify-center w-full mt-4 text-3xl">
  <div className="w-5/6">
    <div class="lg:flex">
      <div className={"w-full lg:w-1/2 h-16 px-4 flex justify-center items-center lg:border-r-8 border-white " + loanAmountLabelBgColor}>
        Loan Amount
      </div>
        <TextBoxWidget name="loanAmount" loanAmount={this.state.loanAmountDisplay} onChange={this.handleChange} bgColor={loanAmountBgColor}/>
    </div>
  </div>
</div>

// AFTER TEXT BOX

class TextBoxWidget extends Component {

  render() {
    return (
      <input
        className={"w-full lg:w-1/2 h-16 text-center " + this.props.bgColor}
        type="text"
        name={this.props.name}
        value={this.props.loanAmount}
        onChange={this.props.onChange}
      />
    );
  }
}
